import json
import threading
import time

def biggestKeyLenInDict(sequenceDict):
    """
    This function returns the biggest length of key in a dict.
    Input: sequenceDict - dict to scan.
    Output: biggestLen - the result.
    """
    biggestLen = len(list(sequenceDict.keys())[0]) if len(sequenceDict) > 0 else None

    for binaryVal, asciiVal in sequenceDict.items():
        if len(binaryVal) > biggestLen:
            biggestLen = len(binaryVal)
    return biggestLen

def readNextLineInBinaryFile(file, line, size):
    """
    This function reads the next line in binary file and saves the size of the line.
    Input: file - opened file, line - the current line, size - the size that the line should have.
    Output: tuple of: result - the result line, flag - if the file has ended.
    """
    oneByte = True
    if (len(line) < size):
        while (len(line) < size) and oneByte:
            oneByte = file.read(1)
            if oneByte:
                line += oneByte.hex() 
    else:
        oneByte = file.read(1)
        if oneByte:
            line = line[2: ] + oneByte.hex()

    return (line, oneByte)

def removeRepeatInList(lst):
    """
    This function removes data repeat in list.
    Input: lst - a python list.
    Output: result - lst without data repeat.
    """
    result = []
    for arg in lst:
        if arg not in result:
            result.append(arg)
    return result

def findMatchesInHex(dstString, srcString, index, output):
    """
    This function gets two hwx values and returns all the occurrences of src in dst.
    Input: srcString - hex value to search, dstString - hex value to search in.
    Output: result - list of all occurrences
    """
    result = []
    dstString = dstString.split(srcString)

    for i in range(0, len(dstString) - 1):
        index += len(dstString[i])
        startIndex = (int)((index + 2) / 2)
        temp = {"range" : (startIndex, startIndex + (int)(len(srcString) / 2)), "size" : len(srcString), "repeating_byte" : str(bytes.fromhex(srcString))}
        index += len(srcString)
        result.append(temp)

    output += result
    
def findMatchingPatternsInBinaryFiles(path, sequenceDict, threadsNumber):
    """
    This function finds matching patterns from sequenceDict(dict of hex values and strings) in file.
    Input: path - a binary file path, sequenceDict(dict of hex values and strings), threadsNumber - number of threads to split the file to.
    Output: json format of the matching patterns.
    """
    try:
        file = open(path, "rb")
    except Exception as e:
        print ("There where problems loading the file, please make sure that the path is logical and the file is binary.")
        return 1
    
    sizeToRead = biggestKeyLenInDict(sequenceDict)
    fileData, flag = readNextLineInBinaryFile(file, "", sizeToRead)
    output = []
    index = 0

    threads = []
    while flag:
        fileData, flag = readNextLineInBinaryFile(file, fileData, sizeToRead)
        for hexValue, asciiValue in sequenceDict.items():
            if len(threads) < threadsNumber:
                t = threading.Thread(target=findMatchesInHex, args=(fileData, hexValue, index,output, ))
                threads.append(t)
                t.start()
            else:
                findMatchesInHex(fileData, hexValue, index,output)
        index += 2
        if index % 100000 == 0:
            print (index, fileData)

    output = removeRepeatInList(output)
    file.close()
    return json.dumps(output)
    
def main():
    THREAD_NUMBER = 0
    PATH = "sample.bin"
    DICT = { '5D00008000': 'lzma',
  '27051956': 'uImage',
  '18286F01': 'zImage',
  '1F8B0800': 'gzip',
  '303730373031': 'cpio',
  '303730373032': 'cpio',
  '303730373033': 'cpio',
  '894C5A4F000D0A1A0A': 'lzo',
  '5D00000004': 'lzma',
  'FD377A585A00': 'xz',
  '314159265359': 'bzip2',
  '425A6839314159265359': 'bzip2',
  '04224D18': 'lz4',
  '02214C18': 'lz4',
  '1F9E08': 'gzip',
  '71736873': 'squashfs',
  '68737173': 'squashfs',
  '51434454': 'dtb',
  'D00DFEED': 'fit',
  '7F454C46': 'elf',
  '300030003a0030003000' : "Hey"
   }
    start = time.time()
    output = findMatchingPatternsInBinaryFiles(PATH, DICT, THREAD_NUMBER)
    end = time.time()
    print ("Calculation time: " + str(end - start) + "[sec]\nOutput:\n" + output)

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print ("Incorrect usage of the code.")
